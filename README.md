
# IDS721 Individual Project 4

## Requirements
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline

## Project Description
This project leverages AWS Lambda functions written in Rust, coordinated through AWS Step Functions, to implement a data processing pipeline that calculates and filters character frequencies in text.

## Lambda Functions
### 1. Calculate Frequency
- **Purpose**: Parses input text in JSON format to calculate the frequency of characters.
- **Output**: Characters and their frequencies sorted lexicographically in a BTreeMap, output as JSON.

### 2. Filter High Frequency
- **Purpose**: Filters characters from the output of `calculate_frequency` that appear three or more times.
- **Output**: Filtered high-frequency characters in a BTreeMap, output as JSON.

## Step Functions Workflow
- **CalculateFrequency**: Invokes the first Lambda function to calculate character frequencies.
- **FilterHighFrequency**: Receives output from `CalculateFrequency` and filters high-frequency characters.
- **State Machine Configuration**:
  ```json
  {
      "Comment": "A workflow to calculate character frequencies and filter high frequencies.",
      "StartAt": "CalculateFrequency",
      "States": {
          "CalculateFrequency": {
              "Type": "Task",
              "Resource": "<MY_FIRST_LAMBDA_ARN>",
              "Next": "FilterHighFrequency"
          },
          "FilterHighFrequency": {
              "Type": "Task",
              "Resource": "<MY_SECOND_LAMBDA_ARN>",
              "End": true
          }
      }
  }
  ```

## Setup and Deployment Instructions
### Create Rust Lambda Project
- Initialize Lambda project with `cargo lambda new <PROJECT_NAME>`.
- Add necessary dependencies in `Cargo.toml`.
- Implement function logic in `main.rs`.

### Testing Locally
- Use `cargo lambda watch` to test Lambda functions locally.
- Specific commands for local testing:
  ```
  # Calculate Frequency
  cd calculate_frequency
  cargo lambda watch
  cargo lambda invoke --data-file input.json
  
  # Filter High Frequency
  cd filter_high_frequency
  cargo lambda watch
  cargo lambda invoke --data-file input.json
  ```

### Deployment on AWS Lambda
- Create a role with `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess`.
- Build the Lambda function:
  ```
  cargo lambda build --release
  ```
- Deploy on AWS:
  ```
  cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>
  ```

### CI/CD Pipeline with GitLab
- Setup `.gitlab-ci.yml` for automated building and deployment.
- Configuration details provided in the project documentation.

## Demo Video
Illustrates the workflow of Step Functions and functionality of the Lambdas. Demonstrated in `demoVideo.mov`.

## Additional Notes
- Ensure environment variables for AWS credentials are correctly configured for deployment.
- Review AWS Lambda and Step Functions documentation for detailed configuration and optimization strategies.

