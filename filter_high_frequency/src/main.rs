use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, BTreeMap};


#[derive(Deserialize, Serialize)]
struct Input {
    result: BTreeMap<char, usize>,
}

#[derive(Deserialize, Serialize)]
struct Output {
    high_frequency: BTreeMap<char, usize>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(filter_frequent_chars);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn filter_frequent_chars(event: Input, _ctx: Context) -> Result<Output, Error> {
    let mut high_frequency = BTreeMap::new();
    for (c, count) in event.result {
        if count >= 3 {
            high_frequency.insert(c, count);
        }
    }
    Ok(Output { high_frequency })
}
